import React from 'react';


export default class User extends React.Component{
	render(){
			return(
				<tr>
					<td>{this.props.id}</td>
					<td>{this.props.name}</td>
					<td>{this.props.email}</td>
					<td ><i className="fa fa-trash" onClick={this._handleClick.bind(this)} ></i></td>
				</tr>
			);
	}

	_handleClick(event){
		event.preventDefault();
		this.props.removeUser(this.props.id);

	}
}