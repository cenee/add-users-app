import React from 'react';

export default class AddUser extends React.Component {

	render(){

		return (
			<div className="myForm">
				<h2>Add user</h2>
				<form onSubmit={this._handleSubmit.bind(this)}>
					<ul>
						<li>
							<label>name: </label>
							<input type="text" placeholder = "Enter name " ref={(input) => this.name = input}></input>
						</li>	
						<li>
							<label>e-mail: </label>
							<input type="email"placeholder = "Enter e-mail " ref={(input) => this.email = input} ></input>
						</li>
					</ul>
					<button>Submit</button>
				</form>
			</div>

		);
	}
	_handleSubmit(event){
				
		event.preventDefault();
		this.props.addUser(this.name.value, this.email.value);
	}
}

