import React from 'react';
import User from './user';
import AddUser from './addUser';


export default class Users extends React.Component{
	constructor(){
		super();
		this.state = {
			users:[{
				name: "Peter",
				email: "ppeterson@gmail.com",
				id: 1
				},{
				name: "Lucas",
				email: "llucason@gmail.com",
				id: 2
				},{
				name: "Andrew",
				email: "asmith@gmail.com",
				id: 3
			}],
			count: 3
		};
	} 
	_getUsers(){
		return this.state.users.map((user) => {
			return (
				<User name={user.name} email = {user.email} id={user.id} key={user.id} removeUser={this._removeUser.bind(this)}/>
			);
		});
	}
	_addUser(name, email){
		this.setState({
			count: this.state.count + 1,
			users: this.state.users.concat(
				[{
					name: name,
					email: email,
					id: this.state.count + 1
				}]
			)
		});

	}
	_removeUser(id){
		const filteredUsers = [];
        // loop over users and put user.id != id
        for (let i = 0; i < this.state.users.length; i++) {
            
            if (id != this.state.users[i].id) {
                filteredUsers.push(this.state.users[i]);
            }
          }
        this.setState({
            users: filteredUsers
        });
    }
	render(){
		return (
			<div className="layout">
				<div className="myTable">
					<h1>User list</h1>
					<table cellspacing="0">
						<thead>
							<tr>
								<th>Id</th>
								<th>Name</th>
								<th>e-mail</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							{this._getUsers()}
						</tbody>
					</table>		 
				</div>
				<AddUser addUser={this._addUser.bind(this)} removeUser={this._removeUser.bind(this)}/>
			</div>
			
		);
	}
}
